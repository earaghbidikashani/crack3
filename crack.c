#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char word[20], hash[40];
};

int qcomp(const void *t, const void *elem){
    struct entry *tt = (struct entry *)t;
    struct entry *ee = (struct entry *)elem;
    return(strcmp(tt->hash, ee->hash));
}

int bcomp(const void *t, const void *elem){
    struct entry *ee = (struct entry *)elem;
    char *tt = (char *)t;
    return(strcmp(tt, ee->hash));
}
// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *f;
    f = fopen(filename, "r");
    *size = 0;
    int length = 10;
    struct entry *hashpass = malloc(length * sizeof(struct entry));
    
    char line[255];
    while(fgets(line, 255, f)!= NULL){
        
        //removing new line
        char *c = line;
        int len = 0;
        while(c != '\0'){
            if(*c == '\n'){
                *c = '\0';
                break;
            }
            c++;
            len++;
        }
        
        //checking to see if there is still space
        //doubling the space if extension is needed
        if(length == *size){
			length *= 2;
			hashpass = realloc(hashpass, length * sizeof(struct entry));
		}
		
		//assigning to array
		char *wr;
        wr = md5(line, len);
        char *s = malloc(20 * sizeof(char));
		strcpy(s, line);
        strcpy(hashpass[*size].word, s);
        strcpy(hashpass[*size].hash, wr);
        
        //printf("%s %s %s\n", hashpass[*size].word, hashpass[*size].hash, s);
        (*size)++;
    }
    return hashpass;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[1]);
        exit(1);
    }

    int size;
    struct entry *dict = read_dictionary(argv[2], &size);
    qsort(dict, size, sizeof(struct entry), qcomp);
    /*
    for(int i = 0; i< size; i++){
        printf("%s %s\n", dict[i].word, dict[i].hash);
    }
    */
    
    FILE *hashes = fopen(argv[1], "r");
    char line[255];
    while(fgets(line, 255, hashes)!= NULL){
        //removing new line
        char *c = line;
        int len = 0;
        while(c != '\0'){
            if(*c == '\n'){
                *c = '\0';
                break;
            }
            c++;
            len++;
        }
        struct entry *found = bsearch(&line, dict, size, sizeof(struct entry), bcomp);
        if(found){
            printf("Word:%s\nHash:%s\n\n", found->word, found->hash);
        }
        else{
            printf("couldn't find!\n");
        }
        
    }
    
}
